﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Plan_Report, App_Web_pzexqqc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">计划报表</li>
          </ol>
        </section>

        <section class="content">

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlPrint" runat="server" NavigateUrl="javascript:McPrintHide('.tab-date');"><span class="label label-primary"><i class="fa fa-print"></i> 打印</span></asp:HyperLink>
                      <asp:LinkButton ID="lnbPrev" runat="server" OnClick="lnbPrev_Click"><span class="label label-back"><i class="fa fa-chevron-left"></i> 上一个</span></asp:LinkButton>
                      <asp:LinkButton ID="lnbNext" runat="server" OnClick="lnbNext_Click"><span class="label label-back"><i class="fa fa-chevron-right"></i> 下一个</span></asp:LinkButton>
                      <asp:Label ID="lblDate1" runat="server"></asp:Label>
                      至
                      <asp:Label ID="lblDate2" runat="server"></asp:Label>
                  </h3>

                  <div class="tab-date">
                      <asp:HyperLink ID="hlDay" runat="server"><span>日</span></asp:HyperLink>
                      <asp:HyperLink ID="hlWeek" runat="server"><span>周</span></asp:HyperLink>
                      <asp:HyperLink ID="hlMonth" runat="server"><span>月</span></asp:HyperLink>
                  </div>

                  <div class="box-tools search-bar">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">
                
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover report-tb" OnRowDataBound="GridView1_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_User") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="部门">
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="成员">
                                <ItemTemplate>
                                    <asp:Label ID="lblUser" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="计划">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="report" />
                                <ItemStyle CssClass="report" Width="400px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="report" />
                                <ItemStyle CssClass="report" Width="100px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="总结">
                                <ItemTemplate>
                                    <asp:Label ID="lblFeedback" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="report" />
                                <ItemStyle CssClass="report" Width="400px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="评价">
                                <ItemTemplate>
                                    <asp:Label ID="lblNote" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="report" />
                                <ItemStyle CssClass="report" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
                
              </div>
            </div>
          </div>

        </section>

      </div>

</asp:Content>


