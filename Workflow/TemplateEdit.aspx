﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Workflow_TemplateEdit, App_Web_3nnyqhi4" %>

<%@ Register Src="~/Controls/KindEditor.ascx" TagName="KindEditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            模板编辑
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">模板编辑</li>
          </ol>
        </section>

        <section class="content">
        
            <div id="AlertDiv" runat="server"></div>

            <div class="box box-default">
                
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                    </h3>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <asp:TextBox ID="txtTemplateName" runat="server" CssClass="form-control" placeholder="名称："></asp:TextBox>
                    </div>
                    <div id="ContentDiv" runat="server">
                    
                        <div class="form-group">
                            <MojoCube:KindEditor id="txtContent" runat="server" Height="500" />
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <asp:CheckBox ID="cbVisible" runat="server" Checked="true"></asp:CheckBox>
                        <label><asp:Label ID="Label2" runat="server" Text="显示"></asp:Label></label>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="cbStepFree" runat="server" Visible="false"></asp:CheckBox>
                        <label><asp:Label ID="Label1" runat="server" Visible="false" Text="自由选择步骤"></asp:Label></label>
                    </div>

                </div>

                <div class="box-footer">
                    <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
                </div>

            </div>

        </section>

      </div>
      
</asp:Content>