﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Mail_List, App_Web_jt4cr5d1" %>

<%@ Register Src="Nav.ascx" TagName="Nav" TagPrefix="MojoCube" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            邮件管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">邮件管理</li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
          
            <MojoCube:Nav id="Nav" runat="server" />
    
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">
                     <asp:Label ID="lblTitle" runat="server"></asp:Label>
                     <asp:LinkButton ID="lnbDeleteAll" runat="server" Visible="false" onclick="lnbDeleteAll_Click" OnClientClick="{return confirm('确定清空回收站吗？');}"><i class="fa fa-trash-o"></i></asp:LinkButton>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body no-padding">
                  <div class="mailbox-controls">
                    <asp:LinkButton ID="lnbSelectAll" runat="server" CssClass="btn btn-default btn-sm checkbox-toggle" onclick="lnbSelectAll_Click" ToolTip="全选/反选"><i class="fa fa-square-o"></i></asp:LinkButton>
                    <div class="btn-group">
                        <asp:LinkButton ID="lnbTrash" runat="server" CssClass="btn btn-default btn-sm" onclick="lnbTrash_Click" ToolTip="放入回收站"><i class="fa fa-trash-o"></i></asp:LinkButton>
                        <asp:LinkButton ID="lnbRead" runat="server" CssClass="btn btn-default btn-sm" onclick="lnbRead_Click" ToolTip="设为已读"><i class="fa fa-eye"></i></asp:LinkButton>
                        <asp:LinkButton ID="lnbStar" runat="server" CssClass="btn btn-default btn-sm" onclick="lnbStar_Click" ToolTip="标为星标"><i class="fa fa-star"></i></asp:LinkButton>
                    </div>
                    <asp:LinkButton ID="lnbRefresh" runat="server" CssClass="btn btn-default btn-sm" onclick="lnbRefresh_Click" ToolTip="刷新"><i class="fa fa-refresh"></i></asp:LinkButton>
                  </div>
                  <div class="table-responsive mailbox-messages">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover table-striped" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Mail") %>'></asp:Label>
                                    <asp:CheckBox ID="cbRead" runat="server" Checked='<%# Bind("IsRead") %>' />
                                    <asp:CheckBox ID="cbAttachment" runat="server" Checked='<%# Bind("IsAttachment") %>' />
                                    <asp:CheckBox ID="cbStar" runat="server" Checked='<%# Bind("IsStar") %>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="选择">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbSelect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="gvStar" runat="server" ToolTip="标记星标" CommandName="_star"><i class="fa fa-star-o text-yellow"></i></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-star" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="类型" SortExpression="TypeID">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("TypeID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="发件人">
                                <ItemTemplate>
                                    <asp:Label ID="lblMailFrom" runat="server" Text='<%# Bind("MailFrom") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-subject" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="收件人">
                                <ItemTemplate>
                                    <asp:Label ID="lblMailTo" runat="server" Text='<%# Bind("MailTo") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-subject" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="主题">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("Subject") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-subject" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="时间">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-date" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="附件">
                                <ItemTemplate>
                                    <asp:Label ID="lblAttachment" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-attachment" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态" SortExpression="StatusID">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvView" runat="server" ToolTip="查看"><span class="label label-primary"><i class="fa fa-search"></i> 查看</span></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <div id="EmptyDiv" runat="server" visible="false" style="height:60px; text-align:center; padding:10px; overflow:auto; border-top:solid 1px #F4F4F4;">暂无信息</div>
                    
                  </div>
                </div>
                <div class="box-footer no-padding" style="margin-top:-20px;">
                  <div class="mailbox-controls">
                  
                    <div id="pager" style="background:#fff; border:0px; margin-top:0px; padding:2px;">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                  </div>
                </div>
              </div>
            </div>

          </div>

        </section>

      </div>

</asp:Content>


