﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Customer_FollowEdit, App_Web_mz0zwgfz" %>

<%@ Register Src="~/Controls/KindEditor.ascx" TagName="KindEditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            患者跟进
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">患者跟进</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
              </h3>
            </div>

            <div class="box-body">

              <div class="row">
              
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label29" runat="server" Text="类型"></asp:Label></label>
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label30" runat="server" Text="状态"></asp:Label></label>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label1" runat="server" Text="标题"></asp:Label></label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group" style="position:relative;">
                    <label><asp:Label ID="Label2" runat="server" Text="患者"></asp:Label></label>
                    <asp:HyperLink ID="hlCustomer" runat="server" Target="_blank" Visible="false"><i class="fa fa-external-link"></i></asp:HyperLink>
                    <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                    <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                    <div style="position:absolute; top:31px; right:20px;">
                        <asp:HyperLink ID="hlSearch" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-search"></i> 查找</span></asp:HyperLink>
                    </div>
                  </div>

                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label31" runat="server" Text="跟进日期"></asp:Label></label>
                    <asp:TextBox ID="txtFollowDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label32" runat="server" Text="下次跟进"></asp:Label></label>
                    <asp:TextBox ID="txtNextFollow" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
              </div>
                
              <div class="row">
                    
                  <div class="form-group" style="padding:15px">
                      <MojoCube:KindEditor id="txtContent" runat="server" Height="500" />
                  </div>
                        
              </div>
                    
            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>