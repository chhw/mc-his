﻿<%@ control language="C#" autoeventwireup="true" inherits="Controls_KindEditor, App_Web_cykmw2tj" %>

<link rel="stylesheet" href="../Controls/KindEditor/themes/default/default.css" />
<link rel="stylesheet" href="../Controls/KindEditor/plugins/code/prettify.css" />
<script charset="utf-8" src="../Controls/KindEditor/kindeditor.js"></script>
<script charset="utf-8" src="../Controls/KindEditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="../Controls/KindEditor/plugins/code/prettify.js"></script>

<script>

    var editor;

    KindEditor.ready(function(K) {
        editor = K.create('#ctl00_cphMain_txtContent_content1',
            {
                uploadJson: '../Controls/KindEditor/asp.net/upload_json.ashx',
                fileManagerJson: '../Controls/KindEditor/asp.net/file_manager_json.ashx',
                filterMode: false
            });
        });

        function printContent() {
            editor.exec('print');
        }

        function insertContent(html) {
            var startOffset = editor.cmd.range.startOffset; //kindEditor中当前光标的位置索引值startOffset
            editor.insertHtml(html); //js插入指定的HTML内容到光标处。
        }
    
</script>

<textarea id="content1" name="content" runat="server"></textarea>